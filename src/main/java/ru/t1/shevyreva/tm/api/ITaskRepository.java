package ru.t1.shevyreva.tm.api;

import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    List<Task> findAll();

    void clear();

    Task findOneById(String Id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    void removeById(String Id);

    void removeByIndex(Integer index);

}
