package ru.t1.shevyreva.tm.api;

import ru.t1.shevyreva.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    List<Project> findAll();

    void clear();

    Project findOneById(String Id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    void removeById(String Id);

    void removeByIndex(Integer index);

}
