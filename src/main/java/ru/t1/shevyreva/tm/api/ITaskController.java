package ru.t1.shevyreva.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

    void showById();

    void showByIndex();

    void showUpdateById();

    void showUpdateByIndex();

    void showRemoveById();

    void showRemoveByIndex();

    void changeTaskStatusByIndex();

    void changeTaskStatusById();

    void completeTaskStatusById();

    void completeTaskStatusByIndex();

    void startTaskStatusById();

    void startTaskStatusByIndex();

}
