package ru.t1.shevyreva.tm.repository;

import ru.t1.shevyreva.tm.api.ITaskRepository;
import ru.t1.shevyreva.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    public Task findOneById(final String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    public Task findOneByIndex(final Integer index) {
        return tasks.get(index);
    }

    public void remove(final Task task) {
        tasks.remove(task);
    }

    public void removeById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return;
        remove(task);
    }

    public void removeByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return;
        remove(task);
    }

}
